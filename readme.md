# Webhop voor Webs2

**Door Dees Oomens**

## Installatie

```
git clone https://dees040@gitlab.com/dees040/avans-webshop.git

cd avans-webshop

composer install

php artisan key:generate

php artisan migrate
```

### Inlog gegevens

Voor admin account:

email: d.oomens@hotmail.nl
wachtwoord: webshop