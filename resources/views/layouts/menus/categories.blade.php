<div class="list-group">
    @if (! isset($selectedCategory))
        <?php $selectedCategory = null; ?>
    @endif

    @foreach($categoryList as $category)
        <a href="{{ route('categories.products', $category) }}"
           class="list-group-item{{ $selectedCategory == $category->id ? ' active' : '' }}">
            {{ $category->name }}
        </a>
    @endforeach
</div>