<div class="col-md-3">
    <div class="list-group">
        <ul class="nav nav-pills nav-stacked">
            {!! menu(\App\Library\Menu\MenuLoader::ADMIN_MENU) !!}
        </ul>
        {{--<a href="{{ route('categories.index') }}" class="list-group-item{{ Request::is('categories', 'categories/*') ? ' active' : '' }}">--}}
            {{--Categories--}}
        {{--</a>--}}
        {{--<a href="{{ route('products.index') }}" class="list-group-item{{ Request::is('products', 'products/*') ? ' active' : '' }}">--}}
            {{--Products--}}
        {{--</a>--}}
        {{--<a href="{{ route('orders.index') }}" class="list-group-item{{ Request::is('orders', 'orders/*') ? ' active' : '' }}">--}}
            {{--Orders--}}
        {{--</a>--}}
        {{--<a href="{{ route('about.edit') }}" class="list-group-item{{ Request::is('admin/about') ? ' active' : '' }}">--}}
            {{--About page--}}
        {{--</a>--}}
    </div>
</div>