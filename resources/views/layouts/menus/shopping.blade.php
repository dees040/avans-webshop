<li class="dropdown">
    <a href="#" class="dropdown-toggle user-cart" data-toggle="dropdown" role="button" aria-expanded="false">
        <span class="glyphicon glyphicon-shopping-cart"></span>
        <span class="cart-items">{{ cart()->total() }}</span>
        - Items
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu dropdown-cart" role="menu">
        @foreach(cart()->items() as $item)
            <li>
                <span class="item">
                    <span class="item-left">
                        <a href="{{ route('products.show', $item->product) }}">
                            <img src="{{ $item->product->getImagePathAttribute() }}" alt="" width="50px"/>
                        </a>
                        <span class="item-info">
                            <a href="{{ route('products.show', $item->product) }}">
                                {{ $item->product->name }} ({{ $item->amount }}x)
                            </a>
                            <span>&euro;{{ $item->product->price }}</span>
                        </span>
                    </span>
                    <a class="item-right" href="{{ route('cart.destroy', $item) }}">
                        <button class="btn btn-xs btn-danger pull-right">x</button>
                    </a>
                </span>
            </li>
        @endforeach
        <li class="divider"></li>
        <li><a class="text-center" href="{{ route('cart.index') }}">View Cart</a></li>
    </ul>
</li>