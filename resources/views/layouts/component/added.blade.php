<div class="modal fade" id="product-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Added to your shopping cart</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="product-info">
                            <div class="pull-left product-thumbnail">
                                <img src="http://placehold.it/150x150" alt="Product" class="product-image" width="150px" height="150px">
                            </div>
                            <div class="pull-left">
                                <a href="#" class="product-link">
                                    <span class="product-name"></span>
                                </a>
                                <div class="product-price"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div>
                            <p>
                                <a href="#" data-dismiss="modal">Continue shopping</a>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 cart-options">
                        <div class="cart">
                            <a href="{{ route('cart.index') }}">Shopping cart</a>
                        </div>
                        <div class="cart-content">
                            <span class="pull-left cart-items"></span>
                            <span class="pull-right cart-costs"></span>
                            <div class="clearfix"></div>
                        </div>
                        <div class="shipping">
                            <span class="pull-left">Shipping: </span>
                            <span class="pull-right text-success">Free</span>
                            <div class="clearfix"></div>
                        </div>
                        <a href="{{ route('cart.index') }}" class="btn btn-success btn-lg">
                            Finish order
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>