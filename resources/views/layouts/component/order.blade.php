<table id="cart" class="table table-hover table-condensed">
    <thead>
    <tr>
        <th style="width:50%">Product</th>
        <th style="width:10%">Price</th>
        <th style="width:8%">Quantity</th>
        <th style="width:22%" class="text-center">Subtotal</th>
        <th style="width:10%"></th>
    </tr>
    </thead>
    <tbody>
    @foreach(cart()->items() as $item)
        <tr data-url="{{ route('cart.update', $item) }}">
            <td data-th="Product">
                <div class="row">
                    <div class="col-sm-2 hidden-xs">
                        <img src="{{ $item->product->image_path }}" alt="Product" class="img-responsive"/>
                    </div>
                    <div class="col-sm-10">
                        <h4 class="nomargin">{{ $item->product->name }}</h4>
                        <p>{{ $item->product->description }}</p>
                    </div>
                </div>
            </td>
            <td data-th="Price">{{ money($item->product->price) }}</td>
            <td data-th="Quantity">
                <input type="number" class="form-control text-center product-amount" value="{{ $item->amount }}" min="1">
            </td>
            <td data-th="Subtotal" class="text-center">{{ money($item->amount * $item->product->price) }}</td>
            <td class="actions" data-th="">
                <a href="{{ route('cart.index') }}" class="btn btn-info btn-sm"><i class="fa fa-refresh"></i></a>
                <a href="{{ route('cart.destroy', $item ) }}" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr class="visible-xs">
        <td class="text-center"><strong>Total {{ money(cart()->totalCosts()) }}</strong></td>
    </tr>
    <tr>
        <td><a href="{{ route('home') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
        <td colspan="2" class="hidden-xs"></td>
        <td class="hidden-xs text-center"><strong>{{ money(cart()->totalCosts()) }}</strong></td>
        <td>
            <form action="{{ route('checkout') }}" method="POST">
                <script
                        src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                        data-key="pk_test_f7XenJlBudaV7yLioEHjA6gI"
                        data-amount="{{ cart()->totalCostsInCents() }}"
                        data-name="DrankShop"
                        data-description="De Drank Shop"
                        data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                        data-locale="auto"
                        data-label="Checkout"
                        data-currency="eur">
                </script>
            </form>
        </td>
    </tr>
    </tfoot>
</table>