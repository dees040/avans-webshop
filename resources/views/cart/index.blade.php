@extends('layouts.app')

@section('content')
    <div class="col-md-12" id="cart-page">
        @include('layouts.component.order')
        @if (count(cart()->items()) == 0)
            <p>
                No items in shopping cart.
            </p>
        @else
            <p>
                De knop hier boven is in testmode! Er zal geen echt geld in rekening gebracht worden. U kunt de volgende
                credit card gebruiken: 4242 4242 4242 4242. Dit is een
                <a href="https://stripe.com/docs/testing#cards">
                    test credit card van stripe.
                </a> De rest van de data mag je zelf bedenken. Ik weet het.. veel gevraagd.
            </p>
        @endif
    </div>
@endsection