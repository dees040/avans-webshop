@extends('layouts.admin')

@section('breadcrumbs')
    {!! Laracrumbs::render() !!}
@endsection

@section('admin-content')
    <div class="col-md-9">
        <p>
            <a href="{{ route('categories.create') }}" class="btn btn-default">Create category</a>
        </p>
        <table class="table table-striped">
            <tr>
                <th>
                    #
                </th>
                <th>
                    Name
                </th>
                <th>
                    Sub Category
                </th>
                <th>
                    Sub Category Of
                </th>
            </tr>
            @foreach($categories as $category)
                <tr>
                    <td>
                        {{ $category->id }}
                    </td>
                    <td>
                        <a href="{{ route('categories.show', $category) }}">
                            {{ $category->name }}
                        </a>
                    </td>
                    <td>
                        @if(is_null($category->subCategoryOf))
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        @else
                            <span class="glyphicon glyphicon-ok"></span>
                        @endif
                    </td>
                    <td>
                        @if(! is_null($category->subCategoryOf))
                            <a href="{{ route('categories.show', $category->subCategoryOf) }}">
                                {{ $category->subCategoryOf->name }}
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $categories->links() }}
    </div>
@endsection