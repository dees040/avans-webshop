@extends('layouts.admin')

@section('breadcrumbs')
    {!! Laracrumbs::render() !!}
@endsection

@section('admin-content')

    <div class="col-md-9">

        <div class="row">

            <form action="{{ route('about.update') }}" method="post">
                {{ method_field('PUT') }}
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('about') ? ' has-error' : '' }}">
                    <label for="about">Description</label>
                    <textarea class="form-control product-description" id="about"
                              name="about">{{ old('about', $about) }}</textarea>

                    @if ($errors->has('about'))
                        <p class="help-block has-error">
                            <strong>{{ $errors->first('about') }}</strong>
                        </p>
                    @endif
                </div>

                <button type="submit" class="btn btn-primary">Update</button>
            </form>

        </div>

    </div>
@endsection
