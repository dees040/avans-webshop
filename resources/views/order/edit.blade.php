@extends('layouts.admin')

@section('breadcrumbs')
    {!! Laracrumbs::render($order) !!}
@endsection

@section('admin-content')
    <div class="col-md-6">
        <form action="{{ route('orders.update', $order) }}" method="post">
            {{ method_field('PUT') }}
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                <label for="status">Status</label>
                <select class="form-control" id="status" name="status">
                    @foreach(['placed', 'shipped', 'delivered', 'unknown'] as $status)
                        <option value="{{ $status }}"{{ $order->status == $status ? ' selected' : '' }}>{{ $status }}</option>
                    @endforeach
                </select>

                @if ($errors->has('status'))
                    <p class="help-block has-error">
                        <strong>{{ $errors->first('status') }}</strong>
                    </p>
                @endif
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection