@extends('layouts.admin')

@section('breadcrumbs')
    {!! Laracrumbs::render() !!}
@endsection

@section('admin-content')
    <div class="col-md-9">
        <table class="table table-striped">
            <tr>
                <th>
                    ID
                </th>
                <th>
                    Placed By
                </th>
                <th>
                    Status
                </th>
                <th>
                    Total costs
                </th>
                <th>
                    Ordered on
                </th>
            </tr>
            @foreach($orders as $order)
                <tr>
                    <td>
                        <a href="{{ route('orders.show', $order) }}">
                            # {{ $order->id }}
                        </a>
                    </td>
                    <td>
                        @if ($order->user)
                            {{ $order->user->name }}
                        @else
                            {{ $order->email }}
                        @endif
                    </td>
                    <td>
                        {{ $order->status }}
                    </td>
                    <td>
                        {{ money($order->total_price) }}
                    </td>
                    <td>
                        {{ $order->created_at }}
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $orders->links() }}
    </div>
@endsection