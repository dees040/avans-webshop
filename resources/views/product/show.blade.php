@extends('layouts.app')

@section('breadcrumbs')
    {!! Laracrumbs::render($product) !!}
@endsection

@section('content')
    <div class="col-md-3">
        <img src="{{ $product->getImagePathAttribute() }}" alt="{{ $product->name }}" width="100%">
    </div>
    <div class="col-md-9">
        <p>
            {{ $product->name }}
        </p>
        <p>
            {{ $product->description }}
        </p>
        <p>
            Price: &euro; {{ $product->price }}
        </p>
        <p>
            In stock: {{ $product->stock }}
        </p>

        <a href="{{ route('products.add', $product->slug) }}" class="btn btn-primary btn-lg add-to-cart"
           data-turbolinks="false">
            <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
        </a>

        @if (is_admin())
            <a href="{{ route('products.edit', $product) }}" class="btn btn-default">Edit</a>

            <a href="{{ route('products.destroy', $product) }}" class="btn btn-warning"
               onclick="event.preventDefault(); document.getElementById('delete-form').submit();">
                Delete
            </a>

            <form id="delete-form" action="{{ route('products.destroy', $product) }}" method="POST" style="display: none;">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
            </form>
        @endif
    </div>
@endsection