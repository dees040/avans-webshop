@extends('layouts.app')

@section('content')
    <div class="col-md-3">
        <h3>Categories found</h3>
        <div class="list-group">
            @foreach($results['categories'] as $category)
                <a href="{{ route('categories.products', $category->slug) }}" class="list-group-item">
                    {{ $category->name }}
                </a>
            @endforeach
        </div>
        @if (! count($results['categories']))
            No categories found.
        @endif
    </div>
    <div class="col-md-9">
        <div class="row">
            <h3>Products found</h3>
            @if (count($results['products']))
                @foreach($results['products'] as $product)
                    @include('layouts.component.product', compact('product'))
                @endforeach
            @else
                <div class="alert alert-warning" role="alert">There where no products found.</div>
            @endif
        </div>
    </div>
@endsection