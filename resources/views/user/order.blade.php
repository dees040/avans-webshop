@extends('layouts.app')

@section('content')
    <table class="table table-striped">
        <tr>
            <th>
                ID
            </th>
            <th>
                Status
            </th>
            <th>
                Total costs
            </th>
            <th>
                Ordered on
            </th>
        </tr>
        @foreach($orders as $order)
            <tr>
                <td>
                    <a href="{{ route('orders.show', $order) }}">
                        # {{ $order->id }}
                    </a>
                </td>
                <td>
                    {{ $order->status }}
                </td>
                <td>
                    {{ money($order->total_price) }}
                </td>
                <td>
                    {{ $order->created_at }}
                </td>
            </tr>
        @endforeach
    </table>
@endsection