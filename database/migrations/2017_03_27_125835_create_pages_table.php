<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('menu_id');
            $table->string('title');
            $table->string('route_name');
            $table->boolean('needs_authentication');
        });

        DB::table('pages')->insert([
            ['menu_id' => 0, 'title' => 'About', 'route_name' => 'about.index', 'needs_authentication' => 0],
            ['menu_id' => 0, 'title' => 'Dashboard', 'route_name' => 'admin.index', 'needs_authentication' => 3],
            ['menu_id' => 1, 'title' => 'Login', 'route_name' => 'login', 'needs_authentication' => 2],
            ['menu_id' => 1, 'title' => 'Register', 'route_name' => 'register', 'needs_authentication' => 2],
            ['menu_id' => 1, 'title' => 'My Orders', 'route_name' => 'users.order', 'needs_authentication' => 1],
            ['menu_id' => 1, 'title' => 'Logout', 'route_name' => 'logout', 'needs_authentication' => 1],
            ['menu_id' => 2, 'title' => 'Products', 'route_name' => 'products.index', 'needs_authentication' => 3],
            ['menu_id' => 2, 'title' => 'Categories', 'route_name' => 'categories.index', 'needs_authentication' => 3],
            ['menu_id' => 2, 'title' => 'Orders', 'route_name' => 'orders.index', 'needs_authentication' => 3],
            ['menu_id' => 2, 'title' => 'About Page', 'route_name' => 'about.edit', 'needs_authentication' => 3],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
