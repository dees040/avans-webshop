<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/about', 'AboutController@index')->name('about.index');
Route::get('/admin/about', 'AboutController@edit')->name('about.edit');
Route::put('/admin/about', 'AboutController@update')->name('about.update');

Route::get('/me', 'User\UserController@show')->name('me');
Route::get('/me/orders', 'User\UserOrderController@index')->name('users.order');

Route::get('/admin', 'Admin\AdminController@index')->name('admin.index')->middleware('auth', 'admin');
Route::get('/admin', 'Admin\AdminController@index')->name('admin.index')->middleware('auth', 'admin');

Route::get('/cart', 'CartController@index')->name('cart.index');
Route::get('/cart/{item}/remove', 'CartController@destroy')->name('cart.destroy');
Route::put('/cart/{item}', 'CartController@update')->name('cart.update');

Route::get('/search', 'SearchController@index')->name('search.index');
Route::post('/search', 'SearchController@index')->name('search.index');

Route::get('categories/{category}/products', 'CategoryProductController@show')->name('categories.products');

Route::resource('products', 'ProductController');
Route::get('products/{product}/add', 'ProductShoppingController@show');
Route::post('products/{product}/add', 'ProductShoppingController@store')->name('products.add');

Route::resource('reviews', 'Product\ProductReviewController');
Route::resource('orders', 'Admin\OrderController');

Route::get('/activate/{token}', 'ActivationController@show')->name('active');

Route::resource('categories', 'Admin\CategoryController', [
    'middleware' => ['auth', 'admin']
]);

Route::post('/cart/pay', 'CheckoutController@store')->name('checkout');
Route::get('/thank-you', 'HomeController@thanks')->name('thank-you');
Route::post('stripe/webhook', '\Laravel\Cashier\Http\Controllers\WebhookController@handleWebhook');