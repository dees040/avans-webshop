<?php

namespace App\Library\Menu;

use App\Models\Page;

class MenuLoader
{
    /**
     * The identifier for the main menu.
     */
    const MAIN_MENU = 0;

    /**
     * The identifier for the auth menu.
     */
    const AUTH_MENU = 1;

    /**
     * The identifier for the sub menu.
     */
    CONST ADMIN_MENU = 2;

    /**
     * The PageParser instance
     *
     * @var \App\Library\Menu\PageParser
     */
    private $parser;

    /**
     * The classes to load.
     *
     * @var string
     */
    private $classes;

    /**
     * MenuLoader constructor.
     *
     * @param  \App\Library\Menu\PageParser  $parser
     */
    public function __construct(PageParser $parser)
    {
        $this->parser = $parser;
    }

    /**
     * Parse a menu to HTML.
     *
     * @param  integer  $menu
     * @return HtmlString
     */
    public function parse($menu)
    {
        $pages = $this->loadMenuItems($menu);

        $html = new HtmlString();

        foreach ($pages as $page) {
            $html->append($this->parser->toHtml($page));
        }

        return $html;
    }

    /**
     * Load the menu items (pages).
     *
     * @param  integer  $menu
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function loadMenuItems($menu)
    {
        return Page::where('menu_id', $menu)->get();
    }

    /**
     * Create the starting tag.
     *
     * @return HtmlString
     */
    private function createStartingTag()
    {
        $classes = 'nav navbar-nav';

        if (! is_null($this->classes)) {
            $classes .= ' ' . $this->classes;
        }

        return new HtmlString("<ul class='{$classes}'>");
    }

    /**
     * Set the classes.
     *
     * @param string  $classes
     */
    public function setClasses($classes)
    {
        $this->classes = $classes;
    }
}