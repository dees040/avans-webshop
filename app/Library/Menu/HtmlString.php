<?php

namespace App\Library\Menu;

use Illuminate\Support\HtmlString as BaseString;

class HtmlString extends BaseString
{
    /**
     * Create a new HTML string instance.
     *
     * @param  string  $html
     * @return void
     */
    public function __construct($html = "")
    {
        parent::__construct($html);
    }

    /**
     * Append HTMl to the HTML string.
     *
     * @param  string  $html
     * @return \App\Library\Menu\HtmlString
     */
    public function append($html)
    {
        $this->html .= $html;

        return $this;
    }
}