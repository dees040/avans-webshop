<?php

namespace App\Library\Cart;

use Auth;
use Uuid;
use Illuminate\Cookie\CookieJar;
use Illuminate\Session\SessionManager;

class CartCaching
{
    /**
     * The Laravel Cookie Jar.
     *
     * @var CookieJar
     */
    private $cookies;

    /**
     * The Laravel Session manager.
     *
     * @var SessionManager
     */
    private $session;

    /**
     * The user identifier.
     *
     * @var string
     */
    private $user;

    /**
     * UserHelper constructor.
     *
     * @param  CookieJar $cookies
     * @param  SessionManager  $session
     */
    public function __construct(CookieJar $cookies, SessionManager $session)
    {
        $this->cookies = $cookies;
        $this->session = $session;
    }

    /**
     * Initialize the user from the auth session,
     * cart session or cart cookie.
     */
    public function initialize()
    {
        if (Auth::check()) {
            $this->user = Auth::user()->id;
        } else if ($this->session->has('cart')) {
            $this->user = $this->session->pull('cart');
        } else if (request()->cookie('cart')) {
            $this->user = request()->cookie('cart');
        } else {
            $this->user = Uuid::generate()->string;
        }

        $this->cacheUser();
    }

    /**
     * Cache the user identifier to session and/or cookies.
     */
    private function cacheUser()
    {
        if ($this->hasCorrectSession()) {
            $this->session->put('cart', $this->user);
        }

        if (! $this->hasCorrectCookie()) {
            $this->cookies->queue('cart', $this->user);
        }
    }

    /**
     * Check if the request has the correct cart session.
     *
     * @return bool
     */
    public function hasCorrectSession()
    {
        if (! $this->session->has('cart')) {
            return false;
        }

        if ($this->session->pull('cart') != $this->user) {
            return false;
        }

        return true;
    }

    /**
     * Check if the request has the correct cart cookie.
     *
     * @return bool
     */
    public function hasCorrectCookie()
    {
        if (! request()->cookie('cart')) {
            return false;
        }

        if (request()->cookie('cart') != $this->user) {
            return false;
        }

        return true;
    }

    /**
     * Check if the given id matches the saved user.
     *
     * @param  string  $id
     * @return bool
     */
    public function compare($id)
    {
        return $this->user == $id;
    }

    /**
     * Set the user.
     *
     * @param  \App\Models\User|string  $user
     * @return $this
     */
    public function setUser($user)
    {
        if ($user instanceof \App\Models\User) {
            $this->user = $user->id;
        } else {
            $this->user = $user;
        }

        return $this;
    }

    /**
     * Convert class to string.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->user;
    }
}