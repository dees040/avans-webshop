<?php

namespace App\Library\Cart;

use App\Models\Order;
use App\Models\Product;
use App\Models\ShoppingList;
use App\Library\Cart\CartCaching;
use App\Models\User;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Contracts\Support\Arrayable;

class Cart implements Arrayable, Jsonable
{
    /**
     * The user identifier.
     *
     * @var \App\Library\Cart\CartCaching
     */
    private $user;

    /**
     * The list of items in shopping cart.
     *
     * @var \Illuminate\Database\Eloquent\Collection
     */
    private $items;

    /**
     * The last item added to shopping cart.
     *
     * @var \App\Models\ShoppingList
     */
    private $lastItem;

    /**
     * Cart constructor.
     *
     * @param  CartCaching  $user
     */
    public function __construct(CartCaching $user)
    {
        $this->user = $user;
    }

    /**
     * Initialize the products.
     *
     * @return \App\Library\Cart\Cart
     */
    public function initialize()
    {
        $this->user->initialize();

        $this->updateItems();

        return $this;
    }

    /**
     * Update the shopping cart items.
     */
    private function updateItems()
    {
        $this->items = ShoppingList::with('product')->where('user_id', $this->user)->get();
    }

    /**
     * @param $product
     * @return \App\Library\Cart\Cart
     */
    public function add($product)
    {
        if ($product instanceof Product) {
            $product = $product->id;
        }

        $item = ShoppingList::where('product_id', $product)
            ->where('user_id', $this->user)
            ->first();

        if (is_null($item)) {
            $item = $this->newItem($product);
        } else {
            $this->updateAmount($item);
        }

        $this->updateItems();
        $this->lastItem = $item->load('product');

        return $this;
    }

    /**
     * Add new item to shopping list.
     *
     * @param  int  $productId
     * @param  int  $amount
     * @return ShoppingList
     */
    private function newItem($productId, $amount = 1)
    {
        $item = ShoppingList::create([
            'product_id' => $productId,
            'user_id' => $this->user,
            'amount' => $amount,
        ]);

        return $item->load('product');
    }

    /**
     * Update the amount of a item in shopping cart.
     *
     * @param ShoppingList $item
     * @param int $amount
     */
    private function updateAmount(ShoppingList $item, $amount = 1)
    {
        $newAmount = $item->amount + $amount;

        $item->update([
           'amount' => $newAmount,
        ]);
    }

    /**
     * Assign the current shopping list to an user.
     *
     * @param  \App\Models\User $user
     * @return \App\Library\Cart\Cart
     */
    public function mergeCarts($user)
    {
        if ($this->user->compare($user->id)) {
            return $this;
        }

        $userCart = new self();
        $userCart->setUser($user->id)->initialize();

        foreach ($this->items as $item) {
            $userItem = $userCart->getProduct($item->product_id);

            if (is_null($userItem)) {
                $userCart->newItem($item->product_id, $item->amount);
            } else {
                $userCart->updateAmount($userItem, $item->amount);
            }
        }

        ShoppingList::where('user_id', $this->user)->delete();

        return $this->initialize();
    }

    /**
     * Get the total products in shopping cart.
     *
     * @return int
     */
    public function total()
    {
        return $this->items->sum->amount;
    }

    /**
     * Get total price of shopping cart.
     *
     * @return int
     */
    public function totalCosts()
    {
        return round($this->calculatePrice(), 2);
    }

    /**
     * Get total price in cents
     *
     * @return int
     */
    public function totalCostsInCents()
    {
        return $this->calculatePrice() * 100;
    }

    /**
     * Calculate the total price.
     *
     * @return int
     */
    private function calculatePrice()
    {
        $price = 0;

        foreach ($this->items as $item) {
            $price += $item->amount * $item->product->price;
        }

        return $price;
    }

    /**
     * Get the items in shopping cart.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function items()
    {
        return $this->items;
    }

    /**
     * The user has payed.
     *
     * @param  string  $email
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function payed($email)
    {
        $order = $this->moveItemsToNewOrder($email);

        $this->items = collect();

        return $order;
    }

    /**
     * Create a new order and moving shopping items to it.
     *
     * @param  string  $email
     * @return \Illuminate\Database\Eloquent\Model
     */
    private function moveItemsToNewOrder($email)
    {
        $order = create_order($email);

        foreach ($this->items as $item) {
            $order->products()->attach($item->product_id, ['amount' => $item->amount]);

            $item->delete();
        }

        return $order;
    }
    
    /**
     * Get an item by product key.
     *
     * @param  $id  int
     * @return \App\Models\ShoppingList
     */
    public function getProduct($id)
    {
        if (count($this->items)) {
            $key = $this->items->search(function ($item, $key) use ($id) {
                return $item->product_id == $id;
            });

            return $this->items->get($key);
        }

        return null;
    }

    /**
     * Set the user.
     *
     * @param  \App\Models\User|string  $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user->setUser($user);

        return $this;
    }

    /**
     * Get the user of the current cart.
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user->__toString();
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'products' => $this->items->toJson(),
            'last' => $this->lastItem,
            'total' => $this->total(),
            'total_price' => $this->totalCosts(),
        ];
    }

    /**
     * Convert the object to its JSON representation.
     *
     * @param  int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }
}