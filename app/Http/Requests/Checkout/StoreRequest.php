<?php

namespace App\Http\Requests\Checkout;

use Mail;
use Stripe\Charge;
use App\Mail\NewOrderPlaced;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Create a new charge.
     */
    public function persist()
    {
        Charge::create([
            'currency' => 'eur',
            'amount' => cart()->totalCostsInCents(),
            'source' => $this->get('stripeToken')
        ], ['api_key' => config('services.stripe.secret')]);

        $order = cart()->payed($this->get('stripeEmail'));

        Mail::send(new NewOrderPlaced($order));
    }
}
