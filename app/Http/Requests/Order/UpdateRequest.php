<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'required|in:placed,shipped,delivered,unknown',
        ];
    }

    /**
     * Update the status of an order.
     */
    public function persist()
    {
        $this->route('order')->update($this->intersect('status'));
    }
}
