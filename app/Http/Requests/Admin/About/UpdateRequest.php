<?php

namespace App\Http\Requests\Admin\About;

use App\Models\Configuration;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'about' => 'required|min:10|max:5000',
        ];
    }

    /**
     * Update the about page.
     */
    public function persist()
    {
        $config = Configuration::findOrFail('about');
        $config->value = $this->about;
        $config->save();
    }
}
