<?php

namespace App\Http\Requests\Product;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:150',
            'category_id' => [
                'required',
                Rule::exists('categories', 'id')
            ],
            'description' => 'required|max:2000',
            'cover' => 'image',
            'price' => 'required|numeric',
            'stock' => 'numeric',
        ];
    }

    /**
     * Update a product.
     *
     * @return \App\Models\Product
     */
    public function persist()
    {
        return $this->route('product')->update(
            $this->intersect('name', 'category_id', 'description', 'price', 'stock')
        );
    }
}
