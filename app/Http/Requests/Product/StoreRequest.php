<?php

namespace App\Http\Requests\Product;

use App\Models\Product;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:150',
            'category_id' => [
                'required',
                Rule::exists('categories', 'id')
            ],
            'description' => 'required|max:2000',
            'cover' => 'required|image',
            'price' => 'required|numeric',
            'stock' => 'numeric',
        ];
    }

    public function persist()
    {
        return Product::create(
            $this->intersect('name', 'category_id', 'description', 'price', 'stock')
        );
    }
}
