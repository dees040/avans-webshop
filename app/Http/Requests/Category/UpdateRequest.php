<?php

namespace App\Http\Requests\Category;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'category_id' => [
                Rule::exists('categories'),
                'not_in:'.$this->route('category')->id,
            ],
        ];
    }

    /**
     * Update the given Category.
     *
     * @return \App\Models\Category
     */
    public function persist()
    {
        return $this->route('category')->update($this->only('name', 'category_id'));
    }
}
