<?php

Laracrumbs::register('home', function ($breadcrumb) {
    $breadcrumb->push('Home', route('home'));
});

Laracrumbs::register('thank-you', function ($breadcrumb) {
    $breadcrumb->parent('home');

    $breadcrumb->push('Thank You', route('thank-you'));
});

Laracrumbs::register('admin.index', function ($breadcrumb) {
    $breadcrumb->parent('home');

    $breadcrumb->push('Dashboard', route('admin.index'));
});

Laracrumbs::register('about.index', function ($breadcrumb) {
    $breadcrumb->parent('home');

    $breadcrumb->push('About', route('about.index'));
});


Laracrumbs::register('about.edit', function ($breadcrumb) {
    $breadcrumb->parent('admin.index');

    $breadcrumb->push('Edit About', route('about.edit'));
});

Laracrumbs::register('categories.index', function ($breadcrumb) {
    $breadcrumb->parent('admin.index');

    $breadcrumb->push('Categories', route('categories.index'));
});

Laracrumbs::register('categories.create', function ($breadcrumb) {
    $breadcrumb->parent('categories.index');

    $breadcrumb->push('Create', route('categories.create'));
});

Laracrumbs::register('categories.show', function ($breadcrumb, $category) {
    $breadcrumb->parent('categories.index');

    $breadcrumb->push($category->name, route('categories.show', $category));
});

Laracrumbs::register('categories.edit', function ($breadcrumb, $category) {
    $breadcrumb->parent('categories.show', $category);

    $breadcrumb->push('Edit', route('categories.edit', $category));
});

Laracrumbs::register('products.index', function ($breadcrumb) {
    $breadcrumb->parent('admin.index');

    $breadcrumb->push('Products', route('products.index'));
});

Laracrumbs::register('products.create', function ($breadcrumb) {
    $breadcrumb->parent('products.index');

    $breadcrumb->push('Create', route('products.create'));
});

Laracrumbs::register('products.show', function ($breadcrumb, $product) {
    $breadcrumb->parent('categories.products', $product->category);

    $breadcrumb->push($product->name, route('products.show', $product));
});

Laracrumbs::register('products.edit', function ($breadcrumb, $product) {
    $breadcrumb->parent('products.show', $product);

    $breadcrumb->push('Edit', route('products.edit', $product));
});

Laracrumbs::register('categories.products', function ($breadcrumb, $category) {
    $breadcrumb->parent('home');

    $breadcrumb->push($category->name, route('categories.products', $category));
});

Laracrumbs::register('orders.index', function ($breadcrumb) {
    $breadcrumb->parent('admin.index');

    $breadcrumb->push('Orders', route('orders.index'));
});

Laracrumbs::register('orders.show', function ($breadcrumb, $order) {
    $breadcrumb->parent('orders.index');

    $breadcrumb->push('Order #'.$order->id, route('orders.show', $order));
});

Laracrumbs::register('orders.edit', function ($breadcrumb, $order) {
    $breadcrumb->parent('orders.show', $order);

    $breadcrumb->push('Edit', route('orders.edit', $order));
});