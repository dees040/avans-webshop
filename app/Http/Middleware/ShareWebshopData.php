<?php

namespace App\Http\Middleware;

use App\Models\Category;
use View;
use Closure;
use App\Models\ShoppingList;

class ShareWebshopData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->shareShoppingCart();

        $this->shareCategories();

        return $next($request);
    }

    /**
     * Share shopping cart to view.
     */
    private function shareShoppingCart()
    {
        cart()->initialize();
    }

    /**
     * Share categories to view.
     */
    private function shareCategories()
    {
        $categories = Category::where('category_id', 0)->get();

        View::share('categoryList', $categories);
    }
}
