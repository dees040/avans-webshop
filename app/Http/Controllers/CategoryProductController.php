<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;

class CategoryProductController extends Controller
{
    /**
     * Get a list of products based on the category.
     *
     * @param Category $category
     * @return \Illuminate\View\View
     */
    public function show(Category $category)
    {
        $products = Product::where('category_id', $category->id)
            ->orWhereIn('category_id', $category->subCategories->pluck('id'))
            ->paginate(12);

        \View::share('selectedCategory', $category->id);
        \View::share('subCategory', $category->category_id != 0);

        return view('category.product', compact('products', 'category'));
    }
}
