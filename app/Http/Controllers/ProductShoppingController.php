<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\Shopping\AddRequest;
use App\Models\Product;

class ProductShoppingController extends Controller
{
    public function show(AddRequest $request, Product $product)
    {
        $request->persist();

        return redirect('/home');
    }

    /**
     * @param AddRequest $request
     * @param Product $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(AddRequest $request, Product $product)
    {
        $request->persist();

        return response()->json(cart());
    }
}
