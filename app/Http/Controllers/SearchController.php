<?php

namespace App\Http\Controllers;

use App\Http\Requests\Search\SearchRequest;

class SearchController extends Controller
{
    /**
     * Search for products.
     *
     * @param SearchRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(SearchRequest $request)
    {
        $results = $request->persist();

        return view('search.index', compact('results'));
    }
}
