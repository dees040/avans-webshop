<?php

namespace App\Providers;

use Schema;
use Laravel\Cashier\Cashier;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootMoneyFormat();

        Schema::defaultStringLength(191);
    }

    /**
     * Boot the correct European money format.
     */
    public function bootMoneyFormat()
    {
        setlocale(LC_MONETARY, 'nl_NL');

        Cashier::useCurrency('eur', '€');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerUploaders();

        $this->registerCart();
    }

    /**
     * Register the image uploaders.
     */
    private function registerUploaders()
    {
        $this->app->bind('app.image', function ($app) {
            return new \App\Library\ImageUpload($app);
        });
    }

    /**
     * Register the cart singleton.
     */
    private function registerCart()
    {
        $this->app->bind('app.cart.user', function ($app) {
            return new \App\Library\Cart\CartCaching($app['cookie'], $app['session']);
        });

        $this->app->singleton('app.cart', function ($app) {
            return new \App\Library\Cart\Cart($app['app.cart.user']);
        });
    }
}
